import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: Color(0x00000000),
      // statusBarBrightness: Brightness.dark
      // statusBarIconBrightness: Brightness.dark
    )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  CupertinoApp(
      title: 'Flutter Demo',
      theme: const CupertinoThemeData(
        brightness: Brightness.light
        // primarySwatch: Colors.blue,
      ),
      home: _WanHomePage(),
    );
  }
}

class _WanHomePage extends StatelessWidget {
  _WanHomePage({super.key});

  final List<String> barTitles = ['home', 'navigation', 'square', 'mine'];

  @override
  Widget build(BuildContext context) {
    Widget scaffold = CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: [
          _BottomNavigationItem(CupertinoIcons.home, barTitles[0]),
          _BottomNavigationItem(CupertinoIcons.compass, barTitles[1]),
          _BottomNavigationItem(CupertinoIcons.square_list, barTitles[2]),
          _BottomNavigationItem(CupertinoIcons.person_circle, barTitles[3]),
        ],
      ),
      tabBuilder: (context, index) {
        String title = barTitles[index];
        return CupertinoTabView(
          builder: (context) {
            return Center(
              child: Text(title,),
            );
          },
        );
      },
    );
    return AnnotatedRegion(
      value: SystemUiOverlayStyle.dark,
      child: scaffold,
    );
  }
}

class _BottomNavigationItem extends BottomNavigationBarItem {

  _BottomNavigationItem(this.iconData, String? label): super(icon: Icon(iconData, size: 24,), label: label);

  final IconData iconData;

}
